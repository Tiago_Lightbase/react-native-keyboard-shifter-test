import React, { PureComponent } from 'react'
import { TextInput, TextStyle, StyleSheet, TextInputProps } from 'react-native'
import { colors } from 'style/colors'

export type Props = TextInputProps & {
  customStyle: TextStyle
}

export default class InputTextField extends PureComponent<Props> {
  render() {
    const { customStyle } = this.props
    return (
      <TextInput
        style={{
          padding: 15,
          width: '100%',
          borderRadius: 4,
          borderWidth: StyleSheet.hairlineWidth,
          borderColor: colors.border,
          backgroundColor: colors.white,
          ...customStyle,
        }}
        {...this.props}
      />
    )
  }
}
