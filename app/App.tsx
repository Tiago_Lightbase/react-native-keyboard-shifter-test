import React, { Component } from 'react'
import { View } from 'react-native'
import InputTextField from 'components/InputTextField'
import { colors } from 'style/colors'
import KeyboardShifter, { InjectedProps } from 'library/react-native-keyboard-shifter/KeyboardShifter'

interface Props {}

export default class App extends Component<Props> {
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: colors.lightGray,
          paddingTop: 60,
          alignItems: 'center',
          paddingHorizontal: 20,
        }}>
        <KeyboardShifter>
          {(injectedProps: InjectedProps) => (
            <>
              <View
                ref={ref => injectedProps.registerElement('header', ref)}
                onLayout={() => injectedProps.onLayoutElemnt('header')}
                style={{ backgroundColor: colors.white, height: 30, width: '70%', marginBottom: 30 }}
              />
              <InputTextField
                ref={ref => injectedProps.registerElement('input1', ref)}
                onLayout={() => injectedProps.onLayoutElemnt('input1')}
                onFocus={() => injectedProps.onFocus('input1')}
                customStyle={{ marginBottom: 30 }}
              />
              <InputTextField
                ref={ref => injectedProps.registerElement('input2', ref)}
                onLayout={() => injectedProps.onLayoutElemnt('input2')}
                onFocus={() => injectedProps.onFocus('input2')}
                customStyle={{ marginBottom: 30 }}
              />
              <InputTextField
                ref={ref => injectedProps.registerElement('input3', ref)}
                onLayout={() => injectedProps.onLayoutElemnt('input3')}
                onFocus={() => injectedProps.onFocus('input3')}
                customStyle={{ marginBottom: 30 }}
              />
              <InputTextField
                ref={ref => injectedProps.registerElement('input4', ref)}
                onLayout={() => injectedProps.onLayoutElemnt('input4')}
                onFocus={() => injectedProps.onFocus('input4')}
                customStyle={{ marginBottom: 30 }}
              />
              <InputTextField
                ref={ref => injectedProps.registerElement('input5', ref)}
                onLayout={() => injectedProps.onLayoutElemnt('input5')}
                onFocus={() => injectedProps.onFocus('input5')}
                customStyle={{ marginBottom: 30 }}
              />
              <View
                ref={ref => injectedProps.registerElement('footer', ref)}
                onLayout={() => injectedProps.onLayoutElemnt('footer')}
                style={{ backgroundColor: colors.white, height: 30, width: '70%' }}
              />
            </>
          )}
        </KeyboardShifter>
      </View>
    )
  }
}
