import React from 'react'
import { Keyboard, KeyboardEventListener, EmitterSubscription, Platform } from 'react-native'
import { element } from 'prop-types'
import { KeyboardShifterElement } from './KeyboardShifterElement.type'

const isIOS = Platform.OS === 'ios'

export interface InjectedProps {
  registerElement: (id: string, ref: any) => void
  onLayoutElemnt: (id: string) => void
  setElementToShow: (id: string) => void
  onFocus: (id: string) => void
}

interface Props {
  children: (childProps: InjectedProps) => React.ReactNode
}

interface State {}

export default class KeyboardShifter extends React.PureComponent<Props, State> {
  keyboardShow?: EmitterSubscription
  keyboardHidde?: EmitterSubscription

  elements: { [id: string]: KeyboardShifterElement } = {}
  elementToShow?: string
  focusElement?: string

  onKeyboardShow = () => {}
  onKeyboardHidde = () => {}

  componentDidMount() {
    this.keyboardShow = Keyboard.addListener(
      isIOS ? 'keyboardWillShow' : 'keyboardDidShow',
      this.onKeyboardShow
    )
    this.keyboardHidde = Keyboard.addListener('keyboardDidHide', this.onKeyboardHidde)
  }

  componentWillUnmount() {
    if (this.keyboardShow) this.keyboardShow.remove()
    if (this.keyboardHidde) this.keyboardHidde.remove()
  }

  registerElement = (id: string, ref: any) => {
    this.elements[id] = {
      ref,
    }
  }

  onLayoutElemnt = (id: string) => {
    const elementInfo = this.elements[id]

    if (elementInfo.ref) {
      elementInfo.ref.measure(
        (x: number, y: number, width: number, height: number, px: number, py: number) => {
          elementInfo.position = {
            x,
            y,
            height,
            width,
          }

          this.elements[id] = elementInfo
        }
      )
    }
  }

  setElementToShow = (id: string) => {
    this.elementToShow = id
  }

  onFocus = (id: string) => {
    this.focusElement = id
  }

  render() {
    const { children } = this.props
    return children({
      registerElement: this.registerElement,
      onLayoutElemnt: this.onLayoutElemnt,
      setElementToShow: this.setElementToShow,
      onFocus: this.onFocus,
    })
  }
}
