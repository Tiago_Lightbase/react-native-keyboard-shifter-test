import { LayoutRectangle } from 'react-native'

export interface KeyboardShifterElement {
  ref: any
  position?: LayoutRectangle
}
